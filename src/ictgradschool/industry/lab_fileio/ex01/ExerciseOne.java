package ictgradschool.industry.lab_fileio.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        int a = 0;


        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        File myFile = new File("input2.txt");

        try (FileReader fr1 = new FileReader(myFile)) {

            while ((a = fr1.read()) != -1){
                total++;

                if (((char) a) == 'e' ||((char) a) == 'E'){
                    numE++;
                }

            }


        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        int a = 0;

        File myFile = new File("input2.txt");

        try (BufferedReader br1 = new BufferedReader(new FileReader(myFile))) {
            while ((a = br1.read()) != -1){
                total++;
                if (((char) a) == 'e' ||((char) a) == 'E'){
                    numE++;
                }
            }


        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
