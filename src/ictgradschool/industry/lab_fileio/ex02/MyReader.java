package ictgradschool.industry.lab_fileio.ex02;

import ictgradschool.Keyboard;

import java.io.*;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        File myFile = new File(fileName + ".txt");

        String print;

        try (BufferedReader br1 = new BufferedReader(new FileReader(myFile))) {
            while ((print = br1.readLine()) != null){
                System.out.println(print);
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
