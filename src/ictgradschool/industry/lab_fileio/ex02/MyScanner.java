package ictgradschool.industry.lab_fileio.ex02;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        File myFile = new File(fileName + ".txt");

        String print;

        try (Scanner s1 = new Scanner(new FileReader(myFile))) {
            while (s1.hasNextLine()){
                System.out.println(s1.nextLine());
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
