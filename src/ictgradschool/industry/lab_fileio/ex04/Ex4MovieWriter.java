package ictgradschool.industry.lab_fileio.ex04;

import ictgradschool.industry.lab_fileio.ex03.Movie;
import ictgradschool.industry.lab_fileio.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter


        File myFile = new File(fileName + ".csv");

        try (PrintWriter pw1 = new PrintWriter(new FileWriter(myFile))) {
              for (int i = 0; i < films.length; i++){
                pw1.print(films[i].getName() + ",");
                pw1.print(films[i].getYear() + ",");
                pw1.print(films[i].getLengthInMinutes() + ",");
                pw1.print(films[i].getDirector() + "");
                pw1.println();
            }
        }

        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
