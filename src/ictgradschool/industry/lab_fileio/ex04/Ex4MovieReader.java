package ictgradschool.industry.lab_fileio.ex04;

import ictgradschool.industry.lab_fileio.ex03.Movie;
import ictgradschool.industry.lab_fileio.ex03.MovieReader;

import java.io.*;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner

        File myFile = new File(fileName + ".csv");
        Movie[] films = new Movie[19];

        String a;
        String b;
        String c;
        String d;

        Boolean isInt = false;

        try (Scanner s1 = new Scanner(new FileReader(myFile))) {
            s1.useDelimiter(",|\\r\\n");

            for(int i = 0; i < films.length; i++){
                a = s1.next();

                b = s1.next();

                c = s1.next();

                d = s1.next();

                Movie newM = new Movie(a, Integer.parseInt(b), Integer.parseInt(c), d);
                films[i] = newM;
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


        return films;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
